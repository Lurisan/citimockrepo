import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component42Component } from './component42/component42.component';
import { Component41Component } from './component41/component41.component';

@NgModule({
  declarations: [Component42Component, Component41Component],
  imports: [
    CommonModule
  ]
})
export class module4Module { }
