import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component22Component } from './component22/component22.component';
import { Component21Component } from './component21/component21.component';

@NgModule({
  declarations: [Component22Component, Component21Component],
  imports: [
    CommonModule
  ]
})
export class module2Module { }
