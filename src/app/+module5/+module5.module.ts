import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component51Component } from './component51/component51.component';
import { Component52Component } from './component52/component52.component';

@NgModule({
  declarations: [Component51Component, Component52Component],
  imports: [
    CommonModule
  ]
})
export class module5Module { }
