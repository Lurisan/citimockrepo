import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path:'+module1',
  loadChildren:'./+module1/+module1.module#module1Module'
},
{
  path:'+module2',
  loadChildren:'./+module2/+module2.module#module2Module'
},
{
  path:'+module3',
  loadChildren:'./+module3/+module3.module#module3Module'
},
{
  path:'+module4',
  loadChildren:'./+module4/+module4.module#module4Module'
},
{
  path:'+module5',
  loadChildren:'./+module5/+module5.module#module5Module'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
