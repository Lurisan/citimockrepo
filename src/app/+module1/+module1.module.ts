import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component11Component } from './component11/component11.component';
import { Component12Component } from './component12/component12.component';

@NgModule({
  declarations: [Component11Component, Component12Component],
  imports: [
    CommonModule
  ]
})
export class module1Module { }
